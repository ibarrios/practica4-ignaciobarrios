package programa;

/*
 * Clse principal en la que se va a ejecutar el programa
 * @author nachobar
 */

import java.util.Scanner;

import clases.Juego;

public class Main {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		System.out.println("Cuantos jugadores quieres crear?");
		int jugadoresCreados = input.nextInt();
		input.nextLine();
		Juego combate = new Juego(jugadoresCreados);

		String opcion;

		do {
			System.out.println("Introduce que opcion del men� quieres ejecutar:\n"
					+ "1.- Crea a los jugadores\n"
					+ "2.- Busca al jugador\n"
					+ "3.- Elimina un jugador\n"
					+ "4.- Lista todos los jugadores\n"
					+ "5.- Cambiar arma de un jugador\n"
					+ "6.- Listar jugadores por las armas\n"
					+ "7.- Un jugador ataca a otro\n"
					+ "8.- Un jugador usa su ataque especial en otro\n"
					+ "9.- Un jugador roba a otro\n"
					+ "10.- Salir del programa\n");

			opcion = input.nextLine();

			switch (opcion) {
			case "1":
				for (int i = 1; i < jugadoresCreados+1; i++) {					
					System.out.println("Introduce el nombre del jugador " + i);
					String nombre = input.nextLine().toLowerCase();
					System.out.println("Introduce el arma, escribe 'Manos' si quieres que luche cuerpo a cuerpo del jugador " + i);
					String arma = input.nextLine().toLowerCase();					
					System.out.println("Introduce su vida del jugador " + i);
					int vida = input.nextInt();
					System.out.println("Introduce el ataque del jugador " + i);
					int ataque = input.nextInt();
					System.out.println("Introduce la defensa del jugador " + i);
					int defensa = input.nextInt();
					combate.nuevoPersonaje(nombre, arma, ataque, defensa, vida);		
					input.nextLine();
				}
				break;
			case "2":
				System.out.println("Busca a un jugador dando su nombre, devuelve un objeto con el jugador. Si no existe lo dice\n"
						+ "Usamos luego un metodo toString para ver el personaje buscado\n");
				System.out.println("Introduce el jugador a buscar\n");			
				String personajeBuscado = input.nextLine().toLowerCase();
				if (combate.buscarPersonaje(personajeBuscado) != null ){
					System.out.println(combate.buscarPersonaje(personajeBuscado.toString()));
				}
				if (combate.buscarPersonaje(personajeBuscado) == null) {
					System.out.println("No se encuentra al personaje");
				}
				break;
			case "3":
				System.out.println("Elimina a uno de los jugadores dando su nombre, si no existe lo dice");
				System.out.println("Introduce el jugador a eliminar\n");
				combate.eliminarPersona(input.nextLine().toLowerCase());
				break;
			case "4":
				System.out.println("Lista todos los personajes disponibles en el momento con sus caracteristicas\n");
				combate.listarTodosPersonajes();
				break;
			case "5":
				System.out.println("Cambiamos el arma de un jugador dando su nombre y el nombre del arma como segundo parametro, si no existe el personaje lo dice");
				System.out.println("El metodo mostrar� por pantalla el resultado");
				System.out.println("Si el jugador no existe lo dice");
				System.out.println("Introduce el jugador al que quieres cambiar el arma");
				String nombre = input.nextLine().toLowerCase();
				System.out.println("Introduce el arma nueva, escribe 'Manos' si quieres que luche cuerpo a cuerpo ");
				String arma = input.nextLine().toLowerCase();
				combate.cambiarArma(nombre, arma);
				break;
			case "6":
				System.out.println("Devuelve las caracteristicas de todos los personajes que tengan un arma dada");
				System.out.println("Si el arma no existe lo dice");
				System.out.println("Introduce el arma que quieres buscar");
				String weapon = input.nextLine().toLowerCase();
				combate.listarPorArma(weapon);
				break;
			case "7":
				System.out.println("Un jugador ataca a otro. Si los jugadores no existen lo dice.");
				System.out.println("El metodo coge el atributo de ataque del atacante, lo guarda en una variable\n"
						+ "y se lo resta primero a la defensa del atacado y luego a su vida total.\n"
						+ "Va mostrando de cuanto va a ser el ataque total y la vida total que le queda al atacado\n");
				System.out.println("Escribe el personaje que ataca");
				String atacante =input.nextLine();
				System.out.println("Escribe el personaje que recibe el ataque");
				String atacado = input.nextLine();
				combate.jugadorAtacaJugador(atacante.toLowerCase(), atacado.toLowerCase());
				break;
			case "8":
				System.out.println("Un jugador usa su ataque especial en otro. Si los jugadores no existen lo dice.");
				System.out.println("Metodo que usa dos numeros aleatorios sobre 5, si los numeros son iguales el jugador atacado muere\n"
						+ "si los numeros son diferentes el jugador atacante muere. Una vez hecho el ataque muestra como han quedado los dos jugadores");
				System.out.println("Escribe el juegador el que lanza el ataque especial");
				String atacanteEspecial = input.nextLine();
				System.out.println("Escribe el jugador que recibe el ataque especial");
				String atacadoEspecial = input.nextLine();
				combate.jugadorUsaAtaqueEspecialEnJugador(atacanteEspecial.toLowerCase(), atacadoEspecial.toLowerCase());
				break;
			case "9":
				System.out.println("Un jugador roba su arma a otro.Si los jugadores no existen lo dice\n"
						+ "Si el jugador robado no tiene ningun arma lo dice\n"
						+ "Si el jugador robado tiene un arma la cambia a 'Manos'"
						+ "El jugador ladron cambia su arma a la que ha robado\n"
						+ "Una vez ejecutado el robo se indica que se ha robado; que ten�an antes y con que se quedan\n");
				System.out.println("Introduce el personaje que roba");
				String ladron = input.nextLine();
				System.out.println("Introduce el personaje al que robas");
				String victima = input.nextLine();
				combate.jugadorRobaArmaAJugador(ladron.toLowerCase(), victima.toLowerCase());
				break;
			case "10":
				System.out.println("El programa va a finalizar");
				break;
			default:
				System.out.println("Opcion incorrecta");
				break;
			}
		} while (!opcion.equals("10"));

		input.close();
	}
}
