package clases;

/*
 * Clase que guarda los atributos de los personajes
 *  @author nachobar
 * 
 */

public class Personaje {
	
	/**
	 * Costructor
	 * genera un nuevo personae dado un nombre
	 * @param nombre es el identificador que tendra el personaje
	 */
	
	
	private String nombre;
	private String arma;
	private int ataque;
	private int defensa;
	private int vida;

	public Personaje(String nombre) {

		this.nombre = nombre;

	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getArma() {
		return arma;
	}

	public void setArma(String arma) {
		this.arma = arma;
	}

	public int getAtaque() {
		return ataque;
	}

	public void setAtaque(int ataque) {
		this.ataque = ataque;
	}

	public int getDefensa() {
		return defensa;
	}

	public void setDefensa(int defensa) {
		this.defensa = defensa;
	}


	public int getVida() {
		return vida;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}

	
	@Override
	public String toString() {
		return "El Personaje se llama: " + nombre + "\n"
				+ "El arma se llama: " + arma + "\n"
				+ "Tiene un ataque de: " + ataque + "\n"
				+ "Tiene una defensa de: " + defensa + "\n"
				+ "Y su vida es: " + vida + "\n";

	}

}
