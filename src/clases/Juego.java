package clases;

/*clase que actua como almacen de personajes
 * @author nachobar
 */


public class Juego {

	private Personaje[] arrayPersonajes;
	
	/*
	 * constructor del juego
	 * @param maxPersonajes recibe un entero que indica el tama�o del array de personajes
	 * 
	 */

	public Juego(int maxPersonajes) {

		this.arrayPersonajes = new Personaje[maxPersonajes];
	}

	/**
	 * da de alta un nuevo personaje
	 * @param nombre que tendra el personaje
	 * @param arma del personaje
	 * @param ataque del personaje
	 * @param defensa del personaje
	 * @param vida del personaje
	 */
	
	
	public void nuevoPersonaje(String nombre,String arma,int ataque,int defensa,int vida) {

		for (int i = 0; i < arrayPersonajes.length; i++) {
			if (arrayPersonajes[i] == null) {
				arrayPersonajes[i] = new Personaje(nombre);
				arrayPersonajes[i].setArma(arma);
				arrayPersonajes[i].setAtaque(ataque);
				arrayPersonajes[i].setDefensa(defensa);
				arrayPersonajes[i].setVida(vida);
				break;
			}
		}
	}

	/* busca un personaje introduciendo su nombre
	 * @param nombre nombre del personaje
	 * @return objeto personaje
	 */
	
	public Personaje buscarPersonaje(String nombre) {

		for (int i = 0; i < arrayPersonajes.length; i++) {
			if (arrayPersonajes[i] != null ) {
				if (arrayPersonajes[i].getNombre().equals(nombre)) {
					return arrayPersonajes[i];
				} 
			} else {
				System.out.println("No hay personajes creados");
			}
		}
		return null; 
	}

	
	/* elimina un personaje introduciendo su nombre
	 * @param nombre nombre del personaje
	 */
	
	public void eliminarPersona(String nombre) {
		
		int contadorRaro = 0;

		for (int i = 0; i < arrayPersonajes.length; i++) {
			if (arrayPersonajes[i] != null ) {
				if (arrayPersonajes[i].getNombre().equals(nombre)) {
					System.out.println("Ha sido eliminado " + arrayPersonajes[i].getNombre());
					arrayPersonajes[i] = null;			
					break;
				} else {
					contadorRaro++;
				}
			} else {
				System.out.println("No hay personajes creados");
			}
		}
		if (contadorRaro == arrayPersonajes.length) {
			System.out.println("El personaje no existe");
		}
	}

	/*lista todos los personajes dados de alta en el array*/
	
	public void listarTodosPersonajes() {
		
		for (int i = 0; i < arrayPersonajes.length; i++) {
			if (arrayPersonajes[i] != null ) {
				System.out.println(arrayPersonajes[i].toString());
			} else {
				System.out.println("No hay ningun personaje creado");
			}
		}
	}
	
	/*cambia el arma de un personaje leido por su nombre que introducimos por otro arma que tambi�n introducimos
	 * @param nombrePersonaje el nombre del personaje a buscar
	 * @param armaNueva el arma que sustituye a la del personaje
	 */

	public void cambiarArma(String nombrePersonaje, String armaNueva) {
		
		int contadorRaro = 0;

		for (int i = 0; i < arrayPersonajes.length; i++) {
			if (arrayPersonajes[i] != null ) {
				if (arrayPersonajes[i].getNombre().equals(nombrePersonaje)) {
					arrayPersonajes[i].setArma(armaNueva);
					System.out.println(arrayPersonajes[i].toString());
					break;
				} else {
					contadorRaro++;
				}
			} else {
				System.out.println("No hay personajes creados");
			}
		}

		if (contadorRaro == arrayPersonajes.length) {
			System.out.println("El personaje no existe");
		}
	}
	
	/*listar atributos de todos los personajes que tengan un arma dada
	 * 
	 * @param arma de los personajes 
	 */
	
	public void listarPorArma(String arma) {

		int contadorRaro = 0 ;

		for (int i = 0; i < arrayPersonajes.length; i++) {
			if (arrayPersonajes[i] != null ) {
				if (arrayPersonajes[i].getArma().equals(arma)) {
					System.out.println(arrayPersonajes[i].toString());

				} else {
					System.out.println("El arma no existe");
				}
			} else {
				System.out.println("No hay personajes creados");
			}
		}

		if (contadorRaro == arrayPersonajes.length) {
			System.out.println("El arma no existe");
		}

	}
	
	/*un personaje introducido por teclado ataca a otro introducido por teclado
	 * 
	 * @param nombrePersonajeAtacante nombre del personaje que lanza el ataqu
	 * @param nombrePersonajeAtacado nombre del personaje atacado
	 */

	public void jugadorAtacaJugador(String nombrePersonajeAtacante, String nombrePersonajeAtacado) {

		int ataqueEmitido = 0;

		for (int i = 0; i < arrayPersonajes.length; i++) {

			if (arrayPersonajes.length != 1) {
				if(arrayPersonajes[i].getNombre().equals(nombrePersonajeAtacante)) {
					ataqueEmitido = arrayPersonajes[i].getAtaque();
					break;
				} else {
					System.out.println("El personaje atacante no existe");
					break;
				}
			} else {
				System.out.println("Solamente hay un jugador");

			}

		}


		int vida = 0;
		int contadorRaro=0;
		for (int i = 0; i < arrayPersonajes.length; i++) {
			if (arrayPersonajes.length != 1 ) {
				if (arrayPersonajes[i].getNombre().equals(nombrePersonajeAtacado)) {
					vida = arrayPersonajes[i].getVida() - (ataqueEmitido-arrayPersonajes[i].getDefensa());
					arrayPersonajes[i].setVida(vida);
					System.out.println(nombrePersonajeAtacante + " va a quitarle " + (ataqueEmitido-arrayPersonajes[i].getDefensa()) + ""
							+ " de vida a " + nombrePersonajeAtacado);
					System.out.println(" A " + nombrePersonajeAtacado + " le quedan " + arrayPersonajes[i].getVida() + " puntos de vida");
					break;
				} else {
					contadorRaro++;
				}
			} else {
				System.out.println("Solamente hay un jugador");
			}

		}
		if (contadorRaro == arrayPersonajes.length) {
			System.out.println("El personaje atacado no existe");
		}

	}


	/*un personaje introducido por teclado ataca con un ataque especial a otro introducido por teclado
	 * 
	 * @param nombrePersonajeAtacante nombre del personaje que lanza el ataque
	 * @param nombrePersonajeAtacado nombre del personaje atacado
	 */


	public void jugadorUsaAtaqueEspecialEnJugador(String nombrePersonajeAtacante, String nombrePersonajeAtacado) {

		int contadorRaro1=0;
		int contadorRaro2=0;

		for (int i = 0; i < arrayPersonajes.length; i++) {
			if (arrayPersonajes[i].getNombre().equals(nombrePersonajeAtacante)) {
				int componenteAtacante = i;
				int tiradaRandom1=(int) (Math.round((Math.random()*4)+1));
				int tiradaRandom2 = (int) (Math.round((Math.random()*4)+1));
				for (int j = 0; j < arrayPersonajes.length; j++) {
					if (arrayPersonajes[j].getNombre().equals(nombrePersonajeAtacado)) {
						if (tiradaRandom1 == tiradaRandom2){
							System.out.println("Tu ataque especial ha sido efectivo " + nombrePersonajeAtacado + " va a morir");
							arrayPersonajes[j].setVida(arrayPersonajes[j].getVida() - arrayPersonajes[j].getVida());
							System.out.println("A " + nombrePersonajeAtacado + " le quedan " + arrayPersonajes[j].getVida() + " puntos de vida");

							break;
						} else {
							System.out.println("Has fallado el ataque especial " + nombrePersonajeAtacante + " va a morir");
							arrayPersonajes[componenteAtacante].setVida(arrayPersonajes[componenteAtacante].getVida() - arrayPersonajes[componenteAtacante].getVida());
							System.out.println("A " + nombrePersonajeAtacante + " le quedan " + arrayPersonajes[componenteAtacante].getVida() + " puntos de vida");
							break;
						}
					} else {
						contadorRaro2++;
					}
				}
			}	else {
				contadorRaro1++;
			}
		}

		if (contadorRaro1 == arrayPersonajes.length) {
			System.out.println("El personaje atacador no existe");
		}
		if (contadorRaro2 == arrayPersonajes.length) {
			System.out.println("El personaje atacado no existe");
		}

	}

	/*un personaje introducido por teclado roba a otro introducido por teclado
	 * 
	 * @param nombrePersonajeLadron nombre del personaje que roba
	 * @param nombrePersonajeRobado nombre del personaje robado
	 */
	
	
	public void jugadorRobaArmaAJugador(String nombrePersonajeLadron, String nombrePersonajeRobado) {

		String armaRobada = "";
		int contadorRaro1 = 0;
		int contadorRaro2 = 0;
		for (int i = 0; i < arrayPersonajes.length; i++) {
			if (arrayPersonajes[i].getNombre().equals(nombrePersonajeLadron)) {
				for (int j = 0; j < arrayPersonajes.length; j++) {
					if (arrayPersonajes[j].getNombre().equals(nombrePersonajeRobado)) {
						if (!arrayPersonajes[j].getArma().equals("manos")) {
							armaRobada = arrayPersonajes[j].getArma();
							arrayPersonajes[j].setArma("manos");
							System.out.println("A " + nombrePersonajeRobado + " le han quitado su " + armaRobada + ""
									+ " y ahora pelea con las manos");
							for (int j2 = 0; j2 < arrayPersonajes.length; j2++) {
								if (arrayPersonajes[j2].getNombre().equals(nombrePersonajeLadron)) {
									System.out.println(nombrePersonajeLadron + " va a dejar de usar su " + arrayPersonajes[j2].getArma() + ""
											+ " para pelear con " + armaRobada);
									arrayPersonajes[j2].setArma(armaRobada);
								}
							}
							break;
						} else {
							System.out.println("El personaje robado no tiene ning�n arma");
						}
					} else {
						contadorRaro1++;
					}
				}
			}	else {
				contadorRaro2++;			
			}
		}

		if (contadorRaro1 == arrayPersonajes.length) {
			System.out.println("El personaje robado no existe");
		}

		if (contadorRaro2 == arrayPersonajes.length) {
			System.out.println("El personaje ladron no existe");
		}
	}
}
